#!/usr/bin/env python3

import pprint
import time
import urllib.parse

import bson
import feedparser
import fire
import newspaper
import praw
import requests
from pymongo import MongoClient

client = MongoClient()

database = client['article-information']
collection = database['articles']


def parse_article(article_url):
    '''Parses an article and returns relevant information about it'''
    document_information = dict(url=article_url)

    # Set up the article
    article = newspaper.Article(article_url)
    article.download()
    article.parse()

    # List of information we want to save about each article
    information = ['authors', 'images', 'movies',
                   'keywords', 'text', 'summary', 'publish_date']

    # Gathering information about the article
    for info in information:
        current_info = getattr(article, info)

        # Sets cannot be saved to the database
        # so we save them as lists instead
        if type(current_info) is set:
            current_info = list(current_info)

        document_information[info] = current_info

    return document_information


def get_comment_tree(comment):
    '''
    Gets all the replies for a certain comment
    and information about each comment
    '''
    # If the comment was deleted
    comment_author = comment.author.name if comment.author else '[removed]'

    comment_data = {
        'id': comment.id,
        'body': comment.body,
        'score': comment.score,
        'author': comment_author,
    }

    # Recursively gets all comments
    replies = [get_comment_tree(reply) for reply in comment.replies]

    comment_data['replies'] = replies

    return comment_data


def scrape_reddit(article_url):
    '''Scrapes Reddit for the given article'''
    reddit = praw.Reddit(client_id='ljgD0mP9iIG28Q',
                         client_secret='QMohj4zSaVv9JgfIYuUrDkJVkI4',
                         user_agent='my user agent')

    print('Reddit posts with the link:')

    reddit_posts = []

    # Gets all (max 5) Reddit posts linking to the given article URL
    for submission in reddit.subreddit('all').search(f"url:{article_url}"):
        current_post = {
            'id': submission.id,
            'title': submission.title,
            'author': submission.author.name,
            'subreddit': submission.subreddit.display_name,
            'score': submission.score,
        }

        print(submission.title)

        current_post_comments = []

        # Gets all comments instead of praw's MoreComments object.
        submission.comments.replace_more(limit=None)

        for comment in submission.comments:
            comment_tree = get_comment_tree(comment)
            current_post_comments.append(comment_tree)

        current_post['comments'] = current_post_comments

        reddit_posts.append(current_post)

    return reddit_posts


class NewsScraper:
    def scrape(self, *args, before=None, after=None, count=10):
        '''
        Main Function

        Scrapes news.google.com for articles with the given keywords
        and saves each article's information to the database.
        '''
        if not args:
            print("""
Usage:      $ python3 main.py scrape [keywords] --count=10 --before=None --after=None

    Searches for news articles from Google using the given keywords

Example:    $ python3 main.py scrape ibm computers --count=1 --before 2014

    Searches for a single article (--count=1)
    about "IBM computers" before 2014 (--before=2014)

Arguments:
    keywords: a list of keys
    count: The maximum number of articles to download
    before: Search for articles before the given date
    after: Search for articles after the given date""")

            return

        # URL encodes each keyword and joins them into part of a URL
        keywords = '+'.join(map(urllib.parse.quote, args))

        # Downloads the RSS and parses it
        news_url = f"https://news.google.com/rss/search?q={keywords}"

        if before is not None:
            news_url += '+before:' + str(before)

        if after is not None:
            news_url += '+after:' + str(after)

        print(news_url)
        ### IPYTHON EMBED ###
        from IPython import embed
        embed()
        ### IPYTHON EMBED ###

        news_request = requests.get(news_url)
        news_feed = feedparser.parse(news_request.text)

        print('--- STARTING SCRAPE ---')

        # For each article it found
        for i, news_entry in enumerate(news_feed['entries'], start=1):
            # If we reached the maximum number of downloads
            if i > count:
                break

            # Gets the URL from the entry
            article_url = news_entry['links'][0]['href']

            print()
            print(f"{i}) Scraping {article_url}... ")

            try:
                document_information = parse_article(article_url)
            except newspaper.article.ArticleException:
                print("Could not download or parse the article!")
                continue

            reddit_data = scrape_reddit(article_url)

            document_information['reddit_data'] = reddit_data
            # Adds the article's information to the database

            collection.insert_one(document_information)

            print('Done!')

        print('--- DONE SCRAPE ---')


    class database:
        def dump(self):
            '''Dumps the whole database'''
            for document in collection.find():
                pprint.pprint(document, depth=1)

        def clear(self):
            '''Deletes every document in the database'''
            collection.delete_many({})


if __name__ == "__main__":
    fire.Fire(NewsScraper)
